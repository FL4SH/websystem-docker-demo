import logo from './logo.svg';
import axios from "axios"
import './App.css';

function App() {
  const makeApiRequest = () => {
    axios.get("api/users").then((response) => {console.log(response)})
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Yaroslav Demchenko222
        </p>
        <button onClick={makeApiRequest}>
          Ok
        </button>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
